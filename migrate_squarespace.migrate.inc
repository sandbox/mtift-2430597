<?php

/**
 * @file
 * Declares the migrations.
 */

/**
 * Implements hook_migrate_api().
 */
function migrate_squarespace_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'squarespace' => array(
        'title' => t('Squarespace Migrations'),
      ),
    ),
    'migrations' => array(
      'SquarespaceBlog' => array(
        'class_name' => 'SquarespaceBlogMigration',
        'group_name' => 'squarespace',
      ),
      'SquarespaceUser' => array(
        'class_name' => 'SquarespaceUserMigration',
        'group_name' => 'squarespace',
      ),
      'SquarespaceUnregisteredUser' => array(
        'class_name' => 'SquarespaceUnregisteredUserMigration',
        'group_name' => 'squarespace',
      ),
    ),
  );
  return $api;
}
