<?php

/**
 * @file
 * Definition of SquarespaceUserMigration.
 */

/**
 * Create Drupal users.
 */
class SquarespaceUserMigration extends SquarespaceMigrationBase {

  // Define XML data.
  protected $item_xpath = '/squarespace-wireframe/website-member-accounts/website-member-account';
  protected $item_ID_xpath = 'id';
  protected $xml_fields = array(
    'login' => 'Login',
    'email' => 'Email',
    //'encrypted-password' => 'Encrypted Password',
    'is-enabled' => 'Is Enabled',
    'deleted' => 'Deleted',
    'is-confirmed' => 'Confirmed',
    'created-on' => 'Created On',
    'last-login-on' => 'Last Login On',
  );

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationUser();

    // The source ID here is the one retrieved from each data item in the XML
    // file, and used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );

    $this->addFieldMapping('name', 'login')
      ->xpath('login');
    $this->addFieldMapping('mail', 'email')
      ->xpath('email');
    // @todo Determine how Squarespace encrypts passwords.
    //$this->addFieldMapping('pass', 'encrypted-password')
      //->xpath('encrypted-password');
    $this->addFieldMapping('status', 'is-enabled')
      ->xpath('is-enabled');
    $this->addFieldMapping('confirmed', 'is-confirmed')
      ->xpath('is-confirmed');
    $this->addFieldMapping('created', 'created-on')
      ->xpath('created-on');
    $this->addFieldMapping('login', 'last-login-on')
      ->xpath('last-login-on');

  }

  public function prepareRow($row) {
		// Skip deleted users.
		if ($row->xml->deleted == 'true') {
			return FALSE;
		}
	}

  public function prepare($account, $row) {
    // Status in Drupal is 1 (not "true").
		$account->status = ($account->status == 'true') ? 1 : 0;
    // Set the timezone to the current timezone, since Squarespce doesn't store this.
		$account->timezone = date_default_timezone_get();
	}
}

