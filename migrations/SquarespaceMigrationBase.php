<?php

/**
 * @file
 * Definition of SquarespaceMigrationBase.
 */

/**
 * Provides a base class for the Squarespace migrations.
 */
class SquarespaceMigrationBase extends XMLMigration {

  // Path relative to document.
  protected $item_xpath = NULL;
  // Each individual record.
  protected $item_ID_xpath = NULL;
  // There isn't a consistent way to automatically identify appropriate
  // "fields" from an XML feed, so pass an explicit list of source fields.
  protected $xml_fields = NULL;

  public function __construct(array $arguments = array()) {
    parent::__construct($arguments);

    $xml_folder = DRUPAL_ROOT . '/sites/default/files/xml/';
    $items_url = $xml_folder . 'squarespace.xml';

    // Make sure the relevant arguments have been set.
    if (empty($this->item_xpath)) {
      if (!isset($arguments['item_xpath'])) {
        throw new Exception('Item Xpath argument not defined.');
      }
      $this->item_xpath = $arguments['item_xpath'];
    }
    if (empty($this->item_ID_xpath)) {
      if (!isset($arguments['item_ID_xpath'])) {
        throw new Exception('Item ID Xpath argument not defined.');
      }
      $this->item_ID_xpath = $arguments['item_ID_xpath'];
    }
    if (empty($this->xml_fields)) {
      if (!isset($arguments['xml_fields'])) {
        throw new Exception('XML fields argument not defined.');
      }
      $this->xml_fields = $arguments['xml_fields'];
    }

    $items_class = new MigrateItemsXML($items_url, $this->item_xpath, $this->item_ID_xpath);
    $this->source = new MigrateSourceMultiItems($items_class, $this->xml_fields);

  }

}
