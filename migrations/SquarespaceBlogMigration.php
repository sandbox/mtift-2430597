<?php

/**
 * @file
 * Definition of SquarespaceBlogMigration.
 */

/**
 * Create blog posts.
 *
 * NOTE: This assumes that blog posts are context element type 2. Other types:
 *   11. Menu item.
 *   3. Comments.
 */
class SquarespaceBlogMigration extends SquarespaceMigrationBase {

  // Define XML data.
  protected $item_xpath = '/squarespace-wireframe/update-log-entries/update-log-entry';
  protected $item_ID_xpath = 'context-element-id';
  protected $xml_fields = array(
    'title' => 'Title',
    'description' => 'Description',
    'added-on' => 'Added',
    'reference-url' => 'URL',
    'context_element_type' => 'Content Type',
    'registered-author-id' => 'Author ID',
    'externally-visible' => 'Published',
  );

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->dependencies = array('SquarespaceUser');
    $this->destination = new MigrateDestinationNode('article');

    // The source ID here is the one retrieved from each data item in the XML
    // file, and used to identify specific items. Squarespace doesn't give
    // blog posts unique IDs.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'context-element-id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'title')
      ->xpath('title');
    $this->addFieldMapping('body', 'description')
      ->xpath('description');
    $this->addFieldMapping('created', 'added-on')
      ->xpath('added-on');
    $this->addFieldMapping('uid', 'registered-author-id')
      ->xpath('registered-author-id')
      ->sourceMigration('SquarespaceUser');

  }

  public function prepareRow($row) {
		// Only process type 2 (blog) elements.
    if ($row->xml->{'context-element-type'} <> 2) {
      return FALSE;
    }
	}

  public function prepare($node, $row) {

    // Published status is 1 not true.
    $node->status = ($node->status == 'true') ? 1 : 0;

    // Decode anything that is base64 encoded.
    if ($row->xml->description['encoding'] == 'base64') {
			$node->body[LANGUAGE_NONE][0]['value'] = base64_decode($row->description);
		}
	}
}

