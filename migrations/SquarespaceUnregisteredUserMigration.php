<?php

/**
 * @file
 * Definition of SquarespaceUnregisteredUserMigration.
 */

/**
 * Create Drupal users from "unregistered authors".
 */
class SquarespaceUnregisteredUserMigration extends SquarespaceMigrationBase {

  // Define XML data.
  protected $item_xpath = '/squarespace-wireframe/unregistered-authors/unregistered-author';
  protected $item_ID_xpath = 'id';
  protected $xml_fields = array(
    'name' => 'Name',
    'mail' => 'email',
  );

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->destination = new MigrateDestinationUser();

    // The source ID here is the one retrieved from each data item in the XML
    // file, and used to identify specific items
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );

    $this->addFieldMapping('name', 'name')
      ->xpath('name')
      ->dedupe('users', 'name');
    $this->addFieldMapping('mail', 'email')
      ->xpath('email');

  }

  public function prepareRow($row) {
		// Skip existing users.
    if (empty($row->xml->name)) {
			return FALSE;
    }
	}

  public function prepare($account, $row) {
    // Make these accounts active.
		$account->status = 1;
    // Set the timezone to the current timezone, since Squarespce doesn't store this.
		$account->timezone = date_default_timezone_get();
	}
}

